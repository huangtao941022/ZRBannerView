//
//  ZRCollectionViewCell.m
//  简书:https://www.jianshu.com/u/043e94ca450f
//
//  Created by 黄涛 on 2018/5/18.
//  Copyright © 2018年 zero. All rights reserved.
//  

#import "ZRCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface ZRCollectionViewCell ()

@property (nonatomic , strong) UIImageView * imgView;
@property (nonatomic , strong) UILabel * titleLabel;

@end
@implementation ZRCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        [self.contentView addSubview:self.imgView];
        [self.contentView addSubview:self.titleLabel];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];

    CGFloat w = self.frame.size.width;
    CGFloat h = self.frame.size.height;
    CGFloat label_Y = self.textAlign == NSTextAlignmentCenter ?  h - 30 - 15 : h - 30;

    _imgView.frame = self.bounds;
    _titleLabel.frame = CGRectMake(0, label_Y, w, 30);

}

#pragma mark - ----------------------- 其他方法 -----------------------
#pragma mark - ----------------------- 代理方法 -----------------------

#pragma mark - ---------------------- set & get ----------------------
#pragma mark - 图片
- (UIImageView *)imgView{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] init];
    }
    return _imgView;
}

#pragma mark - 标题
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.numberOfLines = 0;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont systemFontOfSize:14];

    }
    return _titleLabel;
}

#pragma mark - 获取 标题
- (void)setTitle:(NSString *)title{
    _title = title;
    
    if (_textAlign == NSTextAlignmentCenter) {
        self.titleLabel.text = title;
        return;
    }
    
    NSMutableParagraphStyle * paraStyle = [[NSMutableParagraphStyle alloc] init];
    
    paraStyle.alignment = _textAlign;
    
    if (_textAlign == NSTextAlignmentLeft) paraStyle.firstLineHeadIndent = 15;
    if (_textAlign == NSTextAlignmentRight) paraStyle.tailIndent = -15;
    
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithString:title attributes:@{NSParagraphStyleAttributeName : paraStyle }];

    [self.titleLabel setAttributedText:attrStr];
}

#pragma mark - 获取 站位图
- (void)setPlaceHoldImg:(NSString *)placeHoldImg{
    _placeHoldImg = placeHoldImg;
}

#pragma mark - 获取 图片
- (void)setImage:(NSString *)image{
    _image = image;
    
    if ([image hasPrefix:@"http"]) {
        
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:[UIImage imageNamed:self.placeHoldImg]];
        
    }else {
        
        self.imgView.image = [UIImage imageNamed:image];
    }
}

#pragma mark - 文字对其方式
- (void)setTextAlign:(NSTextAlignment)textAlign{
    _textAlign = textAlign;
    _titleLabel.textAlignment = textAlign;
    
}

#pragma mark - 文字 背景色
- (void)setIsShowTitleBgColor:(BOOL)isShowTitleBgColor{
    _isShowTitleBgColor = isShowTitleBgColor;
    
    _titleLabel.backgroundColor =  isShowTitleBgColor ? [[UIColor blackColor] colorWithAlphaComponent:0.3] : [UIColor clearColor];
}

#pragma mark - 标题 颜色
- (void)setTitleColor:(UIColor *)titleColor{
    _titleColor = titleColor;
    _titleLabel.textColor = titleColor;
}

@end




