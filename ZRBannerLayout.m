//
//  ZRBannerLayout.m
//  ScreenRead
//
//  Created by 黄涛 on 2018/9/10.
//  Copyright © 2018年 アポロジャパン. All rights reserved.
//

#import "ZRBannerLayout.h"
@interface ZRBannerLayout ()

/** 记录上次滑动停止时contentOffset值 */
@property (nonatomic, assign) CGPoint lastOffset;
/** 滑动每一页的距离 */
@property (nonatomic, assign , readonly) CGFloat pageSpace;


@end
@implementation ZRBannerLayout

- (instancetype)init{
    if (self = [super init]) {
        
        _lastOffset = CGPointZero;
    
    }
    return self;
}

- (void)prepareLayout{
    
    [super prepareLayout];
    
    /**
     * decelerationRate系统给出了2个值：
     * 1. UIScrollViewDecelerationRateFast（速率快）
     * 2. UIScrollViewDecelerationRateNormal（速率慢）
     * 此处设置滚动加速度率为fast，这样在移动cell后就会出现明显的吸附效果
     */
     self.collectionView.decelerationRate = UIScrollViewDecelerationRateFast;
    
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSArray * superAttributes = [super layoutAttributesForElementsInRect:rect];
    NSArray * attributes = [[NSArray alloc] initWithArray:superAttributes copyItems:YES];

    CGFloat centerX = self.collectionView.contentOffset.x + self.collectionView.frame.size.width * 0.5;
    
    [attributes enumerateObjectsUsingBlock:^(UICollectionViewLayoutAttributes *attribute, NSUInteger idx, BOOL * _Nonnull stop) {
        
        // cell的中心点x 和 collectionView最中心点的x值 的间距
       CGFloat spacing = fabs(attribute.center.x - centerX);
        /*移动的距离和屏幕宽度的的比例*/
       CGFloat apartScale = spacing / self.collectionView.bounds.size.width;
        /*把卡片移动范围固定到 -π/4到 +π/4这一个范围内*/
       CGFloat scale = fabs(cos(apartScale * M_PI/4));

        attribute.transform3D = CATransform3DMakeScale(1, scale, 1);

    }];
    
    return attributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds;
{
    return YES;
}

/**
 * 这个方法的返回值，就决定了collectionView停止滚动时的偏移量
 * 这个方法在滚动松手的那一刻调用
 */
- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity
{
    CGFloat currentOffsetX = ABS(proposedContentOffset.x - _lastOffset.x);
    //判断当前滑动方向,手指向左滑动：YES；手指向右滑动：NO
    CGFloat directionSpace = (proposedContentOffset.x - _lastOffset.x) > 0 ?self.pageSpace : - self.pageSpace;

    proposedContentOffset = currentOffsetX > self.pageSpace / 2.5 ? CGPointMake(_lastOffset.x + directionSpace,proposedContentOffset.y):_lastOffset;

    _lastOffset.x = proposedContentOffset.x;
    return proposedContentOffset;

}

- (CGFloat)pageSpace{
    return self.itemSize.width + self.minimumLineSpacing;
}


@end
