//
//  ZRPageControl.m
//  简书:https://www.jianshu.com/u/043e94ca450f
//
//  Created by 黄涛 on 2018/5/18.
//  Copyright © 2018年 zero. All rights reserved.
//  

#import "ZRPageControl.h"

@implementation ZRPageControl

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
     
        
    }
    return self;
}

- (void)setCurrentPage:(NSInteger)currentPage{
    [super setCurrentPage:currentPage];
    
    if (!_isCustom) return;
    if (self.numberOfPages == 0) return;
    for (int i = 0; i < self.subviews.count; i++) {

        UIView * subView = self.subviews[i];

        NSString * imageName = i == currentPage ? self.selectedImgName : self.normalImgName;
        
        if (subView.subviews.count == 0) {
            UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(subView.bounds.origin.x, subView.bounds.origin.x, 10, 10)];
            [subView addSubview:imageView];

        }

        UIImageView * imageView = subView.subviews[0];
        UIImage * image = [UIImage imageNamed:imageName];

//        CGRect frame = imageView.frame;
//        frame.size = CGSizeMake(30, 30);
//        subView.frame = frame;

        imageView.image = image;
        subView.layer.cornerRadius = 0;
        subView.layer.masksToBounds = YES;
        subView.backgroundColor = [UIColor clearColor];


//        imageView.size = image.size;
      

     }
    
    
    


}




#pragma mark - ---------------------- set & get ----------------------
#pragma mark - 选中图片
- (void)setSelectedImgName:(NSString *)selectedImgName{
    _selectedImgName = selectedImgName;
    self.isCustom = YES;
}

#pragma mark - 正常图片
- (void)setNormalImgName:(NSString *)normalImgName{
    _normalImgName = normalImgName;
    self.isCustom = YES;
}

#pragma mark - 是否为自定义
- (void)setIsCustom:(BOOL)isCustom{
    _isCustom = isCustom;
}

#pragma mark - pageSize
- (void)setPageIconSize:(CGSize)pageIconSize{
    _pageIconSize = pageIconSize;
}

@end
