//
//  ZRPageControl.h
//  简书:https://www.jianshu.com/u/043e94ca450f
//
//  Created by 黄涛 on 2018/5/18.
//  Copyright © 2018年 zero. All rights reserved.
//  

#import <UIKit/UIKit.h>

@interface ZRPageControl : UIPageControl

/** 选中图片名称 设置后 isCustom = YES */
@property (nonatomic , copy) NSString * selectedImgName;
/** 正常图片名称 设置后 isCustom = YES */
@property (nonatomic , copy) NSString * normalImgName;
/** 是否为自定义样式 */
@property (nonatomic , assign) BOOL isCustom;
/** PageSize  图片 大小 */
@property (nonatomic , assign) CGSize pageIconSize;

@end
