//
//  ZRCollectionViewCell.h
//  简书:https://www.jianshu.com/u/043e94ca450f
//
//  Created by 黄涛 on 2018/5/18.
//  Copyright © 2018年 zero. All rights reserved.
//  

#import <UIKit/UIKit.h>

@interface ZRCollectionViewCell : UICollectionViewCell

@property (nonatomic , copy) NSString * title;

@property (nonatomic , copy) NSString * image;

@property (nonatomic , copy) NSString * placeHoldImg;

@property (nonatomic , assign) NSTextAlignment  textAlign;

@property (nonatomic , assign) BOOL isShowTitleBgColor;

@property (nonatomic , assign) UIColor * titleColor;

@end
