//
//  ZRBannerView.h
//  简书:https://www.jianshu.com/u/043e94ca450f
//
//  Created by 黄涛 on 2018/5/18.
//  Copyright © 2018年 zero. All rights reserved.
//  轮播图 核:CollectionView

#import <UIKit/UIKit.h>

typedef enum {
    mid_ZRPosition = 0, //文字在上，pageCtrl在下 居中
    left_ZRPosition,    //文字右对齐 pageCtrl 左对齐
    right_ZRPosition    //文字左对齐 PageCtrl 右对齐
}ZRPageCtrlPosition;

@class ZRBannerView;
@protocol ZRBannerViewDelegate <NSObject>

@optional
/** 轮播图点击 */
- (void)bannerView:(ZRBannerView *)bannerView selectedItemIndex:(NSInteger)selectedIndex;
/** 轮播图滚动 */
- (void)bannerView:(ZRBannerView *)bannerView scrollIndex:(NSInteger)index;

@end

@interface ZRBannerView : UIView

@property (nonatomic, weak) id<ZRBannerViewDelegate> delegate;

/** 是否显示PageCtrl 默认:显示 */
@property (nonatomic , assign) BOOL isShowPageCtrl;
/** 是否自动轮播 默认:自动 */
@property (nonatomic , assign) BOOL isAutoRecycle;
/** 是否是轮播 默认:YES */
@property (nonatomic , assign) BOOL isEndless;
/** 自动轮播时间间隔 默认:3s */
@property (nonatomic , assign) NSTimeInterval autoTimeInterval;
/** PageCtrl 选中颜色 默认: black */
@property (nonatomic , assign) UIColor * pageCtrlSelectedColor;
/** PageCtrl 常规颜色 默认: gray */
@property (nonatomic , assign) UIColor * pageCtrlNomalColor;
/** PageCtrl 位置 */
@property (nonatomic , assign) ZRPageCtrlPosition  pageCtrlPosition;
/** 是否显示标题背景色 默认:No ，如果Yes 显示【黑色 透明0.3】 */
@property (nonatomic , assign) BOOL isShowTitleBgColor;
/** title 颜色 */
@property (nonatomic , assign) UIColor * titleColor;
/** 滚动方向 默认:横向 */
@property (nonatomic , assign) UICollectionViewScrollDirection scrollDirection;
/** 边距: 根据滚动方向 例：【左右滚动->左右边距】 默认:0 */
@property (nonatomic , assign) CGFloat margin;



/** 设置数据 */
- (void)setImages:(NSArray *)images titles:(NSArray *)titles;

@end


